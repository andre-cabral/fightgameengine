﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightersSelection : MonoBehaviour {

	public static FightersSelection control;
	public GameObject character1;
	public GameObject character2;

	void Awake () {
		if(control == null){
			DontDestroyOnLoad(this);
			control = this;
		}else{
			Destroy(gameObject);
		}
	}
}
