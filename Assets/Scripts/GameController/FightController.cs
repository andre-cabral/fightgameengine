﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightController : MonoBehaviour {

	public Transform character1StartPosition;
	GameObject character1Object;
	MovementManager character1MovementManager;
	AttackManager character1AttackManager;

	public Transform character2StartPosition;
	GameObject character2Object;
	MovementManager character2MovementManager;
	AttackManager character2AttackManager;

	void Start(){
		FightersSelection.control.character1.GetComponent<MovementManager> ().isPlayer2 = false;
		character1Object = Instantiate(FightersSelection.control.character1);
		character1Object.transform.position = character1StartPosition.position;
		character1MovementManager = character1Object.GetComponent<MovementManager> ();
		character1AttackManager = character1MovementManager.attackManager;

		FightersSelection.control.character2.GetComponent<MovementManager> ().isPlayer2 = true;
		character2Object = Instantiate(FightersSelection.control.character2);
		character2Object.transform.position = character2StartPosition.position;
		character2MovementManager = character2Object.GetComponent<MovementManager> ();
		character2AttackManager = character2MovementManager.attackManager;
		character2MovementManager.isPlayer2 = true;

		character1MovementManager.setEnemyObject (character2Object);
		character1MovementManager.setEnemyAttackManager (character2AttackManager);
		character1MovementManager.setEnemyMovementManager (character2MovementManager);
		character2MovementManager.setEnemyObject (character1Object);
		character2MovementManager.setEnemyMovementManager (character1MovementManager);
		character2MovementManager.setEnemyAttackManager (character1AttackManager);
	}
}
