﻿using UnityEngine;
using System.Collections;

public class HashAnimatorCharacter : MonoBehaviour {

	public int xVelocity;
	public int yVelocity;
	public int grounded;
	public int attackIndex;
	public int crouching;
	public int hit;
	public int fall;
	public int block;

	void Awake () {
		xVelocity = Animator.StringToHash("xVelocity");
		yVelocity = Animator.StringToHash("yVelocity");
		grounded = Animator.StringToHash("Grounded");
		attackIndex = Animator.StringToHash ("AttackIndex");
		crouching = Animator.StringToHash ("Crouching");
		hit = Animator.StringToHash ("Hit");
		fall = Animator.StringToHash ("Fall");
		block = Animator.StringToHash ("Block");
	}

}
