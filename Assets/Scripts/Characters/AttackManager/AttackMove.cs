﻿using UnityEngine;
using System.Collections;

public class AttackMove : MonoBehaviour {

	public string moveName;
	public ButtonsNames[] buttonsControls;
	public ButtonsNames[] buttonsControlsReversed;
	private string moveControlsString = ""; 
	public string MoveControlsString{ get{return moveControlsString; }}
	private string moveControlsStringReversed = ""; 
	public string MoveControlsStringReversed{ get{return moveControlsStringReversed; }}
	public float attackDelayTimeAir = 1f;
	public float attackDelayTimeStand = 1f;
	public float attackDelayTimeCrouch = 1f;
	public int attackIndex = 0;
	public bool canUseAir = true;
	public bool canUseStand = true;
	public bool canUseCrouch = true;
	public int damageAir = 1;
	public int damageStand = 1;
	public int damageCrouch = 1;
	public float knockBackSpeedAir = 0.5f;
	public float knockBackSpeedStand = 0.5f;
	public float knockBackSpeedCrouch = 0.5f;
	public float knockBackTimeAir = 0.1f;
	public float knockBackTimeStand = 0.1f;
	public float knockBackTimeCrouch = 0.1f;
	public float dizzyTimeAir = 0.2f;
	public float dizzyTimeStand = 0.2f;
	public float dizzyTimeCrouch = 0.2f;
	public bool makeFallAir = false;
	public bool makeFallStand = false;
	public bool makeFallCrouch = false;
	AttackManager comboMovesObject;
	MoveControlsData moveDataAir;
	MoveControlsData moveDataStand;
	MoveControlsData moveDataCrouch;

	void Awake(){
		moveControlsString = "";
		foreach (ButtonsNames name in buttonsControls) {
			moveControlsString += name.ToString () + ";";
		}

		moveControlsStringReversed = "";
		foreach (ButtonsNames name in buttonsControlsReversed) {
			moveControlsStringReversed += name.ToString () + ";";
		}

		moveDataAir = new MoveControlsData(attackDelayTimeAir, canUseAir, damageAir, knockBackSpeedAir, knockBackTimeAir, dizzyTimeAir, makeFallAir);
		moveDataStand = new MoveControlsData(attackDelayTimeStand, canUseStand, damageStand, knockBackSpeedStand, knockBackTimeStand, dizzyTimeStand, makeFallStand);
		moveDataCrouch = new MoveControlsData(attackDelayTimeCrouch, canUseCrouch, damageCrouch, knockBackSpeedCrouch, knockBackTimeCrouch, dizzyTimeCrouch, makeFallCrouch);
	}
	
	public virtual void ExecuteMove(){
		
	}

	public MoveControlsData getMoveControlsData(PlayerInstance playerInstance){
		switch (playerInstance) {
			case PlayerInstance.air:
				return moveDataAir;
			case PlayerInstance.stand:
				return moveDataStand;
			case PlayerInstance.crouch:
				return moveDataCrouch;
			default:
				return null;
		}
	}

	public void setComboMovesObject(AttackManager comboMovesObject){
		this.comboMovesObject = comboMovesObject;
	}
}

public class MoveControlsData : MonoBehaviour {
	public float attackDelayTime;
	public bool canUse;
	public int damage;
	public float knockBackSpeed;
	public float knockBackTime;
	public float dizzyTime;
	public bool makeFall;

	public MoveControlsData(float attackDelayTime, bool canUse, int damage, float knockBackSpeed, float knockBackTime, float dizzyTime, bool makeFall){
		this.attackDelayTime = attackDelayTime;
		this.canUse = canUse;
		this.damage = damage;
		this.knockBackSpeed = knockBackSpeed;
		this.knockBackTime = knockBackTime;
		this.dizzyTime = dizzyTime;
		this.makeFall = makeFall;
	}
}