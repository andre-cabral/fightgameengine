﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtBox : MonoBehaviour {

	public AttackManager attackManager;

	void OnTriggerEnter2D(Collider2D other){
		if ((other.tag == Tags.hitBoxP2 && gameObject.tag == Tags.hurtBox)||
			(other.tag == Tags.hitBox && gameObject.tag == Tags.hurtBoxP2)) {
			attackManager.gotHit(other.gameObject.GetComponent<HitBox> ().attackManager.getLastMoveUsedData());
		}
	}
}
