﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour {
	public const string achievementVisualObject = "AchievementVisualObject";
	public const string audioController = "AudioController";
	public const string background = "Background";
	public const string backgroundMusicObject = "BackgroundMusicObject";
	public const string destructibleObject = "DestructibleObject";
	public const string enemy = "Enemy";
	public const string enemyExtraCollider = "EnemyExtraCollider";
	public const string enemyProjectile = "EnemyProjectile";
	public const string hitBox = "HitBox";
	public const string hitBoxP2 = "HitBox_P2";
	public const string hurtBox = "HurtBox";
	public const string hurtBoxP2 = "HurtBox_P2";
	public const string player = "Player";
	public const string playerP2 = "Player_P2";
	public const string playerProjectile = "PlayerProjectile";
	public const string platform = "Platform";
	public const string scenarioObject = "ScenarioObject";
	public const string voiceSoundObject = "VoiceSoundObject";
	public const string wall = "Wall";
}
