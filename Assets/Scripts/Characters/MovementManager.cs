﻿using UnityEngine;
using System.Collections;

public class MovementManager : MonoBehaviour {

	public float jumpStartForce = 3f;
	public float jumpStartTime = 0.15f;
	float jumpStartTimePassed = 0f;
	public float jumpInertiaTime = 0.15f;
	float jumpInertiaTimePassed = 0f;
	public float jumpDirectionVelocity = 2f;
	//public float jumpMidForce = 3f;
	//public float jumpTime = 0.5f;
	public float walkSpeed = 2f;
	public bool blockAir = false;
	public bool reversed = false;
	bool grounded = true;
	bool crouching = false;
	bool touchingGround = true;
	bool touchingCeiling = false;
	float jumpTimePassed = 0f;
	bool jumpPressed = false;
	bool jumpOnStartVelocity = false;
	bool jumpVelocityZero = true;
	Rigidbody2D rbody2d;
	Animator animator;
	HashAnimatorCharacter hashAnimatorCharacter;
	public AttackManager attackManager;
	int attackIndex;
	public GameObject hurtBox;
	public GameObject hitBox;

	public bool isPlayer2 = false;
	public bool isAI = false;

	bool AButton = false;
	bool LeftButton = false;
	bool RightButton = false;

	bool canMove = true;
	bool hit = false;
	bool fall = false;

	string horizontalButton = "Horizontal";
	string verticalButton = "Vertical";
	string quickButton = "Quick";
	string strongButton = "Strong";

	GameObject enemyObject;
	MovementManager enemyMovementManager;
	AttackManager enemyAttackManager;

	public bool lookingRight = true;

	float knockBackSpeed = 0f;
	float knockBackTime = 0f;
	float knockBackTimePassed = 0f;

	void Awake () {
		rbody2d = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
		hashAnimatorCharacter = GetComponent<HashAnimatorCharacter>();

		if(isPlayer2){
			horizontalButton += "_P2";
			verticalButton += "_P2";
			quickButton += "_P2";
			strongButton += "_P2";
			gameObject.tag += "_P2";
			hitBox.tag += "_P2";

			gameObject.layer = LayerMask.NameToLayer(LayerMask.LayerToName(gameObject.layer) + "_P2");
			hitBox.layer = LayerMask.NameToLayer(LayerMask.LayerToName(hitBox.layer) + "_P2");
			hurtBox.tag += "_P2";
			hurtBox.layer = LayerMask.NameToLayer(LayerMask.LayerToName(hurtBox.layer) + "_P2");
		}
	}

	void Update () {
		if (!PauseMenuController.control.pauseAndUnpause.getPaused ()) {

			FlipCheck ();

			if ((jumpPressed || jumpOnStartVelocity) && !grounded) {
				if (jumpStartTimePassed >= jumpStartTime) {
					jumpOnStartVelocity = false;
					jumpTimePassed += Time.deltaTime;
				} else {
					jumpStartTimePassed += Time.deltaTime;
					jumpTimePassed += Time.deltaTime;
				}
			}

			if (!jumpVelocityZero && ((!jumpOnStartVelocity && !jumpPressed && jumpInertiaTimePassed >= jumpInertiaTime) || touchingCeiling)) {
				Vector2 velocity = rbody2d.velocity;
				if (velocity.y > 0f) {
					velocity.y = 0f;
					rbody2d.velocity = velocity;
				}

				jumpTimePassed = 0f;
				jumpStartTimePassed = 0f;

				jumpInertiaTimePassed = 0f;

				jumpVelocityZero = true;
			}

			if (!jumpVelocityZero && (!jumpOnStartVelocity && !jumpPressed && !touchingCeiling && jumpInertiaTimePassed < jumpInertiaTime)) {
				jumpInertiaTimePassed += Time.deltaTime;
			}

			//movement inputs
			if (canMove) {
				if (Input.GetAxisRaw (verticalButton) < 0 && grounded && !crouching) {
					CrouchStart ();
				}
				//Debug.Log (Input.GetAxisRaw (verticalButton));
				if (Input.GetAxisRaw (verticalButton) >= 0 && grounded && crouching) {
					CrouchEnd ();
				}
				//mobile start
				/*
				if (LeftButton && grounded && !crouching) {
					rbody2d.velocity = new Vector2 ((Vector2.right.x * -1 * walkSpeed), rbody2d.velocity.y);
				}

				if (RightButton && grounded && !crouching) {
					rbody2d.velocity = new Vector2 ((Vector2.right.x * 1 * walkSpeed), rbody2d.velocity.y);
				}

				if (!LeftButton && !RightButton) {
					//rbody2d.velocity = new Vector2( 0f, rbody2d.velocity.y );
				}
				*/
				//mobile end

				//desktop start
				/*on touch pressed. subtitute axisraw for -1 or 1*/
				if (Input.GetAxisRaw (horizontalButton) != 0 && grounded && !crouching) {
					rbody2d.velocity = new Vector2 ((Vector2.right.x * Input.GetAxisRaw (horizontalButton) * walkSpeed), rbody2d.velocity.y);
					CheckBlock ();
				}
				if (Input.GetAxisRaw (horizontalButton) == 0 && grounded && !crouching) {
					rbody2d.velocity = new Vector2 (0f, rbody2d.velocity.y);
				}
				if (Input.GetAxisRaw (horizontalButton) != 0 && grounded && crouching) {
					rbody2d.velocity = new Vector2 (0f, rbody2d.velocity.y);
					CheckBlock ();
				}
				if (Input.GetAxisRaw (horizontalButton) != 0 && !grounded && blockAir) {
					CheckBlock ();
				}
			
				if (Input.GetAxisRaw (verticalButton) > 0) {
					JumpButtonStart ();
				}
			
				/*on touch status endtouch*/
				if (Input.GetAxisRaw (verticalButton) <= 0 && jumpPressed) {
					JumpButtonEnd ();
				}
				//desktop end
			} else {
				if (grounded && knockBackSpeed == 0) {
					rbody2d.velocity = new Vector2 (0f, rbody2d.velocity.y);
				}
				if (knockBackSpeed != 0) {
					if (knockBackTimePassed < knockBackTime) {
						knockBackTimePassed += Time.deltaTime;
						if (lookingRight) {
							rbody2d.velocity = new Vector2 ((Vector2.right.x * -1 * knockBackSpeed), rbody2d.velocity.y);
						} else {
							rbody2d.velocity = new Vector2 ((Vector2.right.x * 1 * knockBackSpeed), rbody2d.velocity.y);
						}
					} else {
						knockBackSpeed = 0f;
						knockBackTime = 0f;
						knockBackTimePassed = 0f;
					}
				}
			}
		}
	}

	void FixedUpdate(){

		//start time
		if(jumpOnStartVelocity && !jumpVelocityZero && jumpStartTimePassed < jumpStartTime){
			Vector2 velocity = rbody2d.velocity;
			velocity.y = jumpStartForce;
			rbody2d.velocity = velocity;
		}

		/*ontouch status = pressed*/
		/*
		if(jumpPressed && jumpTimePassed <= jumpTime && !jumpVelocityZero && jumpStartTimePassed >= jumpStartTime){
			Vector2 velocity = rbody2d.velocity;
			velocity.y = jumpMidForce;
			rbody2d.velocity = velocity;
		}
		*/

		if (!reversed) {
			animator.SetFloat (hashAnimatorCharacter.xVelocity, rbody2d.velocity.x);
		} else {
			animator.SetFloat (hashAnimatorCharacter.xVelocity, -rbody2d.velocity.x);
		}
		animator.SetFloat(hashAnimatorCharacter.yVelocity, rbody2d.velocity.y);

		if(rbody2d.velocity.y != 0f && grounded){
			setGrounded(false);
		}

		if(!grounded && rbody2d.velocity.y > -0.1f && rbody2d.velocity.y < 0.1f && touchingGround){
			setGrounded(true);

			Vector2 velocity = rbody2d.velocity;
			velocity.x = 0f;
			rbody2d.velocity = velocity;

			jumpTimePassed = 0f;
			jumpStartTimePassed = 0f;

			//reset canMove after landing
			attackManager.changeCanMove(true, 0f, 999);
		}
	}

	public void CheckBlock(){
		if (lookingRight && Input.GetAxisRaw (horizontalButton) < 0 && enemyMovementManager.getAttackIndex() != 999) {
			StartBlock();

		}
		if (!lookingRight && Input.GetAxisRaw (horizontalButton) > 0 && enemyMovementManager.getAttackIndex() != 999) {
			StartBlock();
		}
	}

	public void StartBlock(){
		animator.SetBool(hashAnimatorCharacter.block, true);
	}

	public void EndBlock(){
		animator.SetBool(hashAnimatorCharacter.block, false);
	}

	public void CrouchStart(){
		crouching = true;
		animator.SetBool(hashAnimatorCharacter.crouching, crouching);
	}

	public void CrouchEnd(){
		crouching = false;
		animator.SetBool(hashAnimatorCharacter.crouching, crouching);
	}

	public void JumpButtonStart(){
		if(grounded){
			jumpTimePassed = 0f;
			jumpStartTimePassed = 0f;

			jumpPressed = true;
			jumpOnStartVelocity = true;
			jumpVelocityZero = false;
			
			Vector2 velocity = rbody2d.velocity;
			velocity.y = jumpStartForce;
			rbody2d.velocity = velocity;
		}
	}

	public void JumpButtonEnd(){
		jumpPressed = false;
	}

	public void setGrounded(bool grounded){
		this.grounded = grounded;
		animator.SetBool(hashAnimatorCharacter.grounded, this.grounded);
	}

	public Rigidbody2D getRigidbody2D(){
		return rbody2d;
	}

	public void setTouchingGround(bool touchingGround){
		this.touchingGround = touchingGround;
	}

	public bool getTouchingGround(){
		return touchingGround;
	}
	
	public void setTouchingCeiling(bool touchingCeiling){
		this.touchingCeiling = touchingCeiling;
	}
	
	public bool getTouchingCeiling(){
		return touchingCeiling;
	}

	public void setLeftButton(bool LeftButton){
		this.LeftButton = LeftButton;
	}
	public void setRightButton(bool RightButton){
		this.RightButton = RightButton;
	}

	public void setCanMove(bool canMove){
		this.canMove = canMove;
	}
	public bool getCanMove(){
		return canMove;
	}

	public void setAttackIndex(int attackIndex){
		this.attackIndex = attackIndex;
		animator.SetInteger(hashAnimatorCharacter.attackIndex, this.attackIndex);
	}

	public int getAttackIndex(){
		return attackIndex;
	}

	public bool getCrouching(){
		return crouching;
	}

	public bool getGrounded(){
		return grounded;
	}

	public void setEnemyObject(GameObject enemyObject){
		this.enemyObject = enemyObject;
	}

	public void setEnemyMovementManager(MovementManager enemyMovementManager){
		this.enemyMovementManager = enemyMovementManager;
	}

	public void setEnemyAttackManager(AttackManager enemyAttackManager){
		this.enemyAttackManager = enemyAttackManager;
	}

	public void FlipCheck(){
		if (enemyObject.transform.position.x > gameObject.transform.position.x && !lookingRight && grounded && canMove) {
			Flip ();
		} 
		if (enemyObject.transform.position.x <= gameObject.transform.position.x && lookingRight && grounded && canMove) {
			Flip ();
		}
	}

	public void Flip(){
		Vector3 oldScale = transform.localScale;
		oldScale.x = oldScale.x * -1;
		transform.localScale = oldScale;

		lookingRight = !lookingRight;

		reversed = !lookingRight;
		attackManager.reversed = reversed;
	}

	public void setKnockBack(float speed, float time){
		knockBackSpeed = speed;
		knockBackTime = time;
		knockBackTimePassed = 0f;
	}

	public void setHit(bool hit){
		this.hit = hit;
		animator.SetBool(hashAnimatorCharacter.hit, this.hit);
	}

	public void setFall(bool fall){
		this.fall = fall;
		animator.SetBool(hashAnimatorCharacter.fall, this.fall);
	}
}
