﻿using UnityEngine;
using System.Collections;

public class ObjectsPooler : MonoBehaviour {

	public GameObject objectPrefab;
	public int numberOfObjects = 5;
	GameObject[] pooledObjects;

	// Use this for initialization
	void Awake () {
		pooledObjects = new GameObject[numberOfObjects];

		for(int i=0; i < pooledObjects.Length; i++){
			pooledObjects[i] = (GameObject)Instantiate(objectPrefab);
			pooledObjects[i].SetActive(false);
		}
	}
	
	public GameObject UsePooledObject(Vector3 positionToUse, Quaternion rotationToUse){
		for(int i=0; i < pooledObjects.Length; i++){
			if(!pooledObjects[i].activeInHierarchy){
				pooledObjects[i].transform.position = positionToUse;
				pooledObjects[i].transform.rotation = rotationToUse;
				pooledObjects[i].SetActive(true);

				return pooledObjects[i];
			}
		}

		return null;
	}
}
