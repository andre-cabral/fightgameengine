﻿using UnityEngine;
using System.Collections;

public class AttackManager : MonoBehaviour {

	public float delayBetweenButtons = 0.1f;
	float delayBetweenButtonsTimePassed = 0f;

	public AttackMove[] moveControls;

	string buttonsPressed;
	string lastStringToUpdate = "";

	public bool reversed = false;

	float timeCantMove = 0f;
	float timeCantMovePassed = 0f;

	public MovementManager movementManager;
	bool canAttack = true;

	AttackMove lastMoveUsed;
	MoveControlsData lastMoveUsedData;

	string horizontalButton = "Horizontal";
	string verticalButton = "Vertical";
	string quickButton = "Quick";
	string strongButton = "Strong";

	void Awake(){
		if(movementManager.isPlayer2){
			horizontalButton += "_P2";
			verticalButton += "_P2";
			quickButton += "_P2";
			strongButton += "_P2";
		}
		foreach(AttackMove moveControl in moveControls){
			moveControl.setComboMovesObject (this);
		}
	}

	void Update () {
		if (!PauseMenuController.control.pauseAndUnpause.getPaused ()) {
			
			string stringToUpdate = "";

			if (timeCantMovePassed < timeCantMove) {
				timeCantMovePassed += Time.deltaTime;
			} else {
				changeCanMove (true, 0f, 999);
			}

			if (canAttack) {
				if (Input.GetAxisRaw (verticalButton) == 1) {
					stringToUpdate += ButtonsNames.up.ToString ();
				}
				if (Input.GetAxisRaw (verticalButton) == -1) {
					stringToUpdate += ButtonsNames.down.ToString ();
				}
				if (Input.GetAxisRaw (horizontalButton) == 1) {
					stringToUpdate += ButtonsNames.right.ToString ();
				}
				if (Input.GetAxisRaw (horizontalButton) == -1) {
					stringToUpdate += ButtonsNames.left.ToString ();
				}

				if (Input.GetButtonDown (quickButton)) {
					if (stringToUpdate != "") {
						if (stringToUpdate + ";" == lastStringToUpdate) {
							stringToUpdate = "";
						} else {
							stringToUpdate += ";";
						}
					}
					stringToUpdate += ButtonsNames.quick.ToString ();
				}
				if (Input.GetButtonDown (strongButton)) {
					if (stringToUpdate != "") {
						if (stringToUpdate + ";" == lastStringToUpdate) {
							stringToUpdate = "";
						} else {
							stringToUpdate += ";";
						}
					}
					stringToUpdate += ButtonsNames.strong.ToString ();
				}
			}

			if (stringToUpdate != "") {
				stringToUpdate += ";";

				if (stringToUpdate == lastStringToUpdate) {
					stringToUpdate = "{PRESSED}";
				} else {
					lastStringToUpdate = stringToUpdate;
					delayBetweenButtonsTimePassed = 0f;
				}
			} else {
				lastStringToUpdate = "";
			}



			if (delayBetweenButtonsTimePassed < delayBetweenButtons) {
				if (stringToUpdate != "{PRESSED}") {
					buttonsPressed += stringToUpdate;
				}
				delayBetweenButtonsTimePassed += Time.deltaTime;
			} else {
				if (stringToUpdate != "{PRESSED}") {
					buttonsPressed = "";
				} else {
					buttonsPressed = lastStringToUpdate;
				}
			}

			//Debug.Log (buttonsPressed);
			foreach (AttackMove moveCheck in moveControls) {
				int moveControlsStringLength = moveCheck.MoveControlsString.Length;
				string moveControlsString = moveCheck.MoveControlsString;

				if (reversed) {
					moveControlsStringLength = moveCheck.MoveControlsStringReversed.Length;
					moveControlsString = moveCheck.MoveControlsStringReversed;
				}

				if (moveControlsStringLength <= buttonsPressed.Length) {
					if (buttonsPressed.Substring (buttonsPressed.Length - moveControlsStringLength).Contains (moveControlsString)) {
						lastMoveUsedData = moveCheck.getMoveControlsData (PlayerInstance.stand);
						if (movementManager.getCrouching ()) {
							lastMoveUsedData = moveCheck.getMoveControlsData (PlayerInstance.crouch);
						}
						if (!movementManager.getGrounded ()) {
							lastMoveUsedData = moveCheck.getMoveControlsData (PlayerInstance.air);
						}

						if (lastMoveUsedData.canUse) {
							buttonsPressed = "";
							moveCheck.ExecuteMove ();
							lastMoveUsed = moveCheck;

							changeCanMove (false, lastMoveUsedData.attackDelayTime, moveCheck.attackIndex);
						} else {
							lastMoveUsedData = null;
						}
					}
				}
			}

		}
	}

	public void changeCanMove(bool canMove, float timeCantMove, int attackIndex){
		this.canAttack = canMove;
		this.timeCantMove = timeCantMove;
		timeCantMovePassed = 0f;
		movementManager.setCanMove (canMove);
		movementManager.setAttackIndex (attackIndex);

		if (canMove) {
			movementManager.setKnockBack (0f, 0f);
			movementManager.setHit (false);
			movementManager.setFall (false);
			movementManager.EndBlock ();
		}
	}

	public void gotHit(MoveControlsData enemyAttack){
		this.canAttack = false;
		this.timeCantMove = enemyAttack.dizzyTime;
		timeCantMovePassed = 0f;

		movementManager.setCanMove (false);
		movementManager.setAttackIndex (999);
		movementManager.setKnockBack (enemyAttack.knockBackSpeed, enemyAttack.knockBackTime);
		movementManager.setHit (true);
		movementManager.setFall (enemyAttack.makeFall);
	}

	public MoveControlsData getLastMoveUsedData(){
		return lastMoveUsedData;
	}
}

public enum ButtonsNames{
	up,
	upleft,
	upright,
	down,
	downleft,
	downright,
	left,
	right,
	quick,
	strong
}

public enum PlayerInstance{
	air,
	stand,
	crouch
}